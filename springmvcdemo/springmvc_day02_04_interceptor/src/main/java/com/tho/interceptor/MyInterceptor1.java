package com.tho.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//实现接口没提示重写方法的原因是因为JDK8的新特性，接口可有直接实现方法
public class MyInterceptor1 implements HandlerInterceptor {
    //预处理，如果返回值是true，放行，执行下一个拦截器，如果没有，执行Controller里面的方法
    //false  不放行
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("MyInterceptor执行了..preHandle111");
        //request.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(request,response);
        return true;
    }

    /*
        后处理
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        System.out.println("MyInterceptor执行了..postHandle111");
        //request.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(request,response);
    }
    /*
        最后执行，页面渲染完后执行,意思也是完成以后的意思，一般用于关闭流，或者关闭用不上的资源
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("MyInterceptor执行了..afterCompletion111");
    }
}

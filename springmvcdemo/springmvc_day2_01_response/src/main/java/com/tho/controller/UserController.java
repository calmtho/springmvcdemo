package com.tho.controller;

import com.tho.domain.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/user")
@RestController
public class UserController {

    @RequestMapping("/testString")
    public String testString(Model model) {
        System.out.println("testString执行了..");
        //模拟从数据库里面拿数据出来
        User user = new User();
        user.setAge(15);
        user.setUsername("wangwu");
        user.setPassword("123");
        //封装model对象
        model.addAttribute("user1", user);
        return "success";
    }

    /*
        方法类型是void
        请求转发，只有一次请求，服务器内部找资源，路径要写全，但是不需要加项目名
        使用重定向是再次发送请求，即请求服务器两次,请求是无法访问服务器内部资源所以一般都是重定向首页
     */
    @RequestMapping("/testVoid")
    public void testVoid(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("testVoid执行了..");
        //转发编写
        //request.getRequestDispatcher("/WEB-INF/pages/success.jsp").forward(request,response);

        //使用重定向
        //response.sendRedirect(request.getContextPath()+"/index.jsp");

        //直接操作进行响应页面步骤
        //先要解决中文乱码问题
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        //响应内容
        response.getWriter().print("你好！！欢迎访问");
        return;//return ;代表到此结束此方法代码
    }

    /*
        使用ModelAndView,底层封装的也是ModelMap，作用于上面的Model和ModelMap作为参数用法差不多
        都是返回视图和模型
     */
    @RequestMapping("testModelAndView")
    public ModelAndView testModelAndView(){
        ModelAndView mv=new ModelAndView();
        System.out.println("执行了testModelAndView。。");
        //模拟从数据库里面拿数据出来
        User user = new User();
        user.setAge(16);
        user.setUsername("xiaoliu");
        user.setPassword("123");
        //把user对象存储到mv对象中，也会把user对象存入request域中
        mv.addObject("user1",user);
        System.out.println(user);
        //指定视图名走的也是视图解析器，所以视图解析器必须配置,参数是视图名，同样找的是我们配置的前缀后缀
        mv.setViewName("success");
        return mv;
    }

    /*
        使用关键字实现转发和重定向
     */
    @RequestMapping("testForwardorRedirect")
    public String testForwardorRedirect(){
        System.out.println("testForwardorRedirect执行了..");

        //请求的转发
        //return "forward:/WEB-INF/pages/success.jsp";
        //重定向
        return "redirect:/index.jsp";
    }

   /* *//*//*
        模拟异步请求响应,测试获取异步请求是否正常
     *//*
    @PostMapping("testAjax")
    public void testAjax(@RequestBody String requestBody){
        System.out.println("testAjax执行了。。。");
        //控制台获取请求体
        System.out.println(requestBody);
    }*/

    /*
        @ResponseBody使用返回Json对象给前端
     */
    @PostMapping("testAjax")
    public @ResponseBody User testAjax(@RequestBody User user){
        System.out.println("testAjax执行了。。。");
        System.out.println("前端获取到的对象： "+user);
        user.setUsername("lisi");
        user.setAge(18);
        System.out.println("返回给前端的的对象： "+user);
        return user;
    }
}

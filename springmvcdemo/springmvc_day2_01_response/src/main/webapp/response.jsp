<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>

    <script src="js/jquery.min.js"></script>
    <script>
        //页面加载，绑定单击事件
        $(function (){
            $("#btn").click(function () {
                //alert("hello btn")
                //发送AJAX请求
                $.ajax({
                    //编写json格式，设置属性和值
                    url:"user/testAjax",
                    contentType:"application/json;charset=UTF-8",
                    data:'{"username":"Ajax","password":"123","age":23}',
                    dataType:"json",
                    type:"post",
                    success:function (data) {
                        //请求发送成功回调data服务器响应json数据，进行解析
                        alert(data)
                        alert(data.username)
                        alert(data.password)
                        alert(data.age)
                    }
                });
            });
        });
    </script>
</head>
<body>
    <a href="user/testString">testString</a>
    <a href="user/testVoid">testVoid</a>
    <a href="user/testModelAndView">testModelAndView</a>
    <a href="user/testForwardorRedirect">testForwardorRedirect</a>
    <br>
    <button id="btn">发送异步请求</button>
</body>

</html>

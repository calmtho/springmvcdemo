package com.tho.controller;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/user")
public class UserController {
    @RequestMapping(value="/fileupload3",method = RequestMethod.POST)
    public String fileupload3(MultipartFile upload) throws Exception {
        System.out.println("SpringMVC跨服务器文件上传。。。");
        //定义一个上传文件服务器的路径
        //String path="http://localhost:9090/uploads/";
        String path="http://localhost:9090/fileuploadserver_war_exploded/uploads/";

        String filename = upload.getOriginalFilename();
        String uuid=UUID.randomUUID().toString().replace("-","");
        filename=uuid+"_"+filename;

        //创建库客户端的对象
        Client client=Client.create();
        //和图片服务器连接
        WebResource webResource= client.resource(path + filename);//这里不用拼接斜杠的原因是上面路径已经带有了
        //上传文件
        webResource.put(upload.getBytes());
        return "success";
    }

    /**
     * SpringMVC文件上传
     * MultipartFile upload ，这个参数必须和前端的选择域的name属性保持一致
     * SpringMVC就会帮我们解析
     */
    @RequestMapping("/fileupload2")
    public String fileupload2(HttpServletRequest request, MultipartFile upload) throws Exception {
        System.out.println("SpringMVC文件上传。。。");

        //使用fileupload组件完成文件上传
        //上传位置
        String path=request.getSession().getServletContext().getRealPath("/uploads/");
        //判断，该路径是否存在
        File file=new File(path);
        if (!file.exists()){
            //不存在就创建该文件夹
            file.mkdirs();
        }
        String filename = upload.getOriginalFilename();
        String uuid=UUID.randomUUID().toString().replace("-","").toUpperCase();
        filename=uuid+"_"+filename;
        //上传文件
        upload.transferTo(new File(file,filename));
        return "success";
    }
    /**
     * 文件上传
     */
    @RequestMapping("/fileupload1")
    public String fileupload1(HttpServletRequest  request) throws Exception {
        System.out.println("文件上传。。。");

        //使用fileupload组件完成文件上传
        //上传位置
        String path=request.getSession().getServletContext().getRealPath("/uploads/");
        //判断，该路径是否存在
        File file=new File(path);
        if (!file.exists()){
            //不存在就创建该文件夹
            file.mkdirs();
        }

        //解析request对象，获取上传文件项
        //先创建磁盘文件工厂，再把工厂放入Servlet文件上传对象中
        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        //解析request
        List<FileItem> items = upload.parseRequest(request);
        //遍历
        for (FileItem item:items){
            //进行判断，当前item对象是否是上传文件项
            if (item.isFormField()){
                //说明普通表单向
            }else {
                //说明上传文件项
                //获取到上传文件的名称
                String filename=item.getName();
                //把文件的名称设置唯一值，uuid
                String uuid= UUID.randomUUID().toString().replace("-","");
                //把uuid和原名进行拼接生成新的名字
                filename=uuid+"_"+filename;
                //完成文件上传
                item.write(new File(path,filename));
                //删除临时文件方法
                item.delete();
            }
        }
        return "success";
    }


}
package com.tho.utils;

import org.springframework.core.convert.converter.Converter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
    把字符串转换城日期的类型转换器
 */
public class StringToDateConverter implements Converter<String, Date>{
/*
    String source传进来的字符串
 */
    @Override
    public Date convert(String source) {
        if (source==null){
            throw new RuntimeException("请您输入日期");
        }
        DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
        try{
            return dateFormat.parse(source);//需要被try,catch
        }catch (Exception e){
            throw new RuntimeException("数据转换出错");
        }


    }
}

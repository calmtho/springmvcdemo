package com.tho.controller;

import com.tho.domain.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import java.util.Date;
import java.util.Map;

/*
    常用注解
 */
@Controller
@SessionAttributes(value={"msg"},types = {String.class})
@RequestMapping("/anno")//放在类上作用相当于路由
public class AnnoController {
    //即使属性名和前端参数属性一致，但没加注解的话，实际上没有绑定起来需要使用@RequestParam("和前端参数一致")
    @GetMapping("/testRequestMapping")
    public String testRequestMapping(@RequestParam("name") String username){
        System.out.println("执行了..");
        System.out.println(username);
        return "success";
    }

    /*
        获取到请求体的内容
     */
    @PostMapping("/testRequestBody")
    public String testRequestBody(@RequestBody String body){
        System.out.println("执行了..");
        System.out.println(body);
        return "success";
    }
    /*
       PathVaribale占位符注解
    */
    @GetMapping("/testPathVaribale/{sid}")
    public String testPathVaribale(@PathVariable(name = "sid") String id){
        System.out.println("执行了..");
        System.out.println(id);
        return "success";
    }

    /*
       PutMapping注解
    */
    @PutMapping("/testPutMapping/{sid}")
    public String testPutMapping(@PathVariable(name = "sid") String id, User user){
        System.out.println("执行了..");
        System.out.println(id);
        System.out.println("put了："+user);
        return "success";
    }

    /*
       DeleteMapping注解
    */
    @DeleteMapping("/testDeleteMapping/{sid}")
    public String testDeleteMapping(@PathVariable(name = "sid") String id, User user){
        System.out.println("执行了..");
        System.out.println(id);
        System.out.println("delete了"+user);
        return "success";
    }

    /*
        获取请求头@RequestHeader，不指定就获取整个，指定就拿对应的
     */
    @RequestMapping("testRequestHeader")
    public String testRequestHeader(@RequestHeader(value = "Accept") String header){
        System.out.println("执行了..");
        System.out.println(header);
        return "success";
    }
    /*
       获取cookie
    */
    @RequestMapping("testCookieValue")
    public String testCookieValue(@CookieValue(value = "JSESSIONID") String cookieValue){
        System.out.println("执行了..");
        System.out.println("JSESSIONID: "+cookieValue);
        return "success";
    }

    /*
        ModelAttribute方法，可在方法以及参数上使用
        在参数上使用绑定Map的键@ModelAttribute("userMap")
     */
    @RequestMapping("testModelAttribute")
    public String testModelAttribute(@ModelAttribute("userMap") User user){
        System.out.println("执行了testModelAttribut..");
        System.out.println(user);
        return "success";
    }
   /* @ModelAttribute
    public User showUser(String uname){
        System.out.println("showUser..+我加了 @ModelAttribute在类上，我先执行");
        User user=new User();
        user.setUname("zhangsan");
        user.setAge(20);
        user.setDate(new Date());
        return user;
    }*/
    @ModelAttribute
    public void showUser(Map<String,User> map){
        System.out.println("showUser..+我加了 @ModelAttribute在类上，我先执行");
        User user=new User();
        user.setUname("zhangsan");
        user.setAge(20);
        user.setDate(new Date());
        map.put("userMap",user);
    }

    /*
        存值到session域
     */
    @RequestMapping(value = "/testSessionAttributes")
    public String testSessionAttributes(Model model){
        System.out.println("testSessionAttributes...");
        //底层会存储到request域对象中
        model.addAttribute("msg","消息");
        return "success";
    }
    /*
        从session域里面取值
     */
    @RequestMapping(value = "/testGetAttributes")
    public String testGetAttributes(ModelMap modelMap){
        System.out.println("testGetAttributes...");
        //使用Map的实现类modelMap获取已经在session域里的值
        String msgStr =(String) modelMap.get("msg");
        System.out.println(msgStr);
        return "success";
    }

    /*
        清除session存入参数对象
     */
    @RequestMapping("testDeleteAttributes")
    public String testDeleteAttributes(SessionStatus status){
        System.out.println("testDeleteAttributes..");
        status.setComplete();
        return "success";
    }
}
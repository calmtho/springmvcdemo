package com.tho.controller;

import com.tho.domain.Account;
import com.tho.domain.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("params")
public class ParamController {

    //可以作用于类或者方法上都可以，path,和value作用相同都是映射路径注解，请求能有写多种请求方式
    //传递可以传参数，也可以传对象等
    @RequestMapping(value = "testparams", method = RequestMethod.GET)
    public String testParam(String username) {
        System.out.println("@RequestMapping起作用了");
        System.out.println("用户名:" + username);
        return "success";
    }

    /**
     * 把用户封装到javaBean的类当中
     *
     * @return
     */
    @RequestMapping(value = "/saveAccount", method = RequestMethod.POST)
    public String saveAccount(Account account) {
        System.out.println("保存了账户");
        System.out.println(account);
        //System.out.println(account.getUser());
        System.out.println(account.getList().toString());
        System.out.println(account.getMap().toString());
        return "success";
    }
    /*
       自定义类型转换器
     */
    @RequestMapping(value = "/saveUser", method = RequestMethod.POST)
    public String saveUser(User user) {
        System.out.println("保存了用户");
        System.out.println(user);
        return "success";
    }
    /*
      访问servlet原生API
     */
    @RequestMapping(value = "/testServlet")
    public String testServlet(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("方法执行了。。");
        System.out.println(request);

        HttpSession session = request.getSession();
        System.out.println(session);

        ServletContext servletContext = session.getServletContext();
        System.out.println(servletContext);


        System.out.println(response);
        return "success";
    }
}

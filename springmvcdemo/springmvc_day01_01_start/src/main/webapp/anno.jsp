<%--
  Created by IntelliJ IDEA.
  User: HaSee
  Date: 2020/10/15
  Time: 0:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"  %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <a href="anno/testRequestMapping?name=张三">RequestMapping</a><br>

    <form action="anno/testRequestBody" method="post">
        用户姓名：<input type="text" name="username"><br/>
        用户年龄：<input type="text" name="age"><br/>
        <input type="submit" value="保存用户，post 请求">
    </form>
    <a href="anno/testPathVaribale/10">PathVaribale</a><br>

    <!-- 更新 -->
    <form action="anno/testPutMapping/10" method="post">
        更新用户：<input type="text" name="uname"><br/>
        <input type="hidden" name="_method" value="PUT"/>
        <input type="submit" value="更新">
    </form>
    <!-- 删除 -->
    <form action="anno/testDeleteMapping/10" method="post">
        删除用户：<input type="text" name="uname"><br/>
        <input type="hidden" name="_method" value="DELETE"/>
        <input type="submit" value="删除">
    </form>

    <a href="anno/testRequestHeader">PathVaribale</a><br>
    <a href="anno/testCookieValue">CookieValue</a><br>

    <form action="anno/testModelAttribute" method="post">
        用户姓名：<input type="text" name="uname"><br/>
        用户年龄：<input type="text" name="age"><br/>
        <input type="submit" value="提交测试ModelAttribute">
    </form>

    <a href="anno/testSessionAttributes">testSessionAttributes</a><br>
    <a href="anno/testGetAttributes">testGetAttributes</a><br>
    <a href="anno/testDeleteAttributes">testDeleteAttributes</a><br>
</body>
</html>